%define debug_package %{nil}

Name:           inframap
Version:        0.7.0
Release:        0%{?dist}
Summary:        Read your tfstate or HCL to generate a graph specific for each provider
Group:          Applications/System
License:        MIT
URL:            https://github.com/cycloidio/inframap
Source0:        https://github.com/cycloidio/%{name}/archive/v%{version}.tar.gz
Source1:        https://github.com/cycloidio/%{name}/releases/download/v%{version}/%{name}-linux-amd64.tar.gz

%description
Read your tfstate or HCL to generate a graph 
specific for each provider, showing only the 
resources that are most important/relevant.

%prep
%autosetup -n %{name} -c

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 %{name}-linux-amd64 $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license LICENSE
%doc CHANGELOG.md README.md
%{_bindir}/%{name}

%changelog
* Mon Jun 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Thu Sep 15 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM